# No.1
The database that I have been designing in this project is from Clash Royale. Clash Royale is a real-time, head-to-head battle game in Clash Universe which developed by Supercell. We can build our own deck with many unique and various cards and use it to play another player in the arena. To get a victory, we have to put our cards correctly to destroy the enemy King and Princesses Tower in a strategic and fast-paced match.

## Design

![image](https://gitlab.com/praktikum-basis-data/tugas-besar-database/-/raw/main/clash_royale_database.png)

# No.2
DDL stands for Data Definition Language  is a set of command to create a new database structure. Some of them are CREATE, ALTER, and DROP. I have given some examples of them below based on my project.

## CREATE TABLE
CREATE TABLE is used to create a database.

```sql
CREATE TABLE `playerprofile` (
  `player_tag` varchar(255) PRIMARY KEY,
  `player_name` varchar(255),
  `player_deck` varchar(255),
  `player_tier` varchar(255),
  `player_tierRatings` varchar(255),
  `player_currentTrophy` integer,
  `player_previousTier` varchar(255),
  `player_clan` varchar(255),
  `player_badge` varchar(255),
  `player_win` integer,
  `player_threecrownwins` integer,
  `player_highestTrophies` integer,
  `player_cardsfound` integer,
  `player_totaldonations` integer,
  `player_favcard` varchar(255)
);

CREATE TABLE `shop` (
  `shop_id` varchar(255) PRIMARY KEY,
  `goldBalance` integer,
  `gemBalance` integer,
  `passroyale` varchar(255),
  `eventbundle` varchar(255),
  `specialoffer` varchar(255),
  `trader` varchar(255),
  `dailydeals` varchar(255),
  `emotes` varchar(255),
  `royalchest` varchar(255),
  `gems` varchar(255),
  `gold` varchar(255),
  `creatorboost` varchar(255)
);

CREATE TABLE `passRoyale` (
  `passRoyale_id` varchar(255) PRIMARY KEY,
  `passRoyale_name` varchar(255),
  `passRoyale_price` integer,
  `passRoyale_buyer` varchar(255)
);

CREATE TABLE `eventBundle` (
  `eventBundle_id` varchar(255) PRIMARY KEY,
  `eventBundle_name` varchar(255),
  `eventBundle_price` integer,
  `eventBundle_buyer` varchar(255)
);

CREATE TABLE `specialOffer` (
  `specialOffer_id` varchar(255) PRIMARY KEY,
  `specialOffer_name` varchar(255),
  `specialOffer_price` integer
);

CREATE TABLE `trader` (
  `trader_id` varchar(255) PRIMARY KEY,
  `trader_card` varchar(255),
  `roll_price` integer
);

CREATE TABLE `dailyDeals` (
  `dailyDeals_id` varchar(255) PRIMARY KEY,
  `dailyDeals_card` varchar(255),
  `dailyDeals_price` integer,
  `dailyDeals_count` integer
);

CREATE TABLE `emoteShop` (
  `emoteShop_id` varchar(255) PRIMARY KEY,
  `emoteShop_name` varchar(255),
  `emoteShop_price` integer
);

CREATE TABLE `royalChests` (
  `royalChests_id` varchar(255) PRIMARY KEY,
  `royalChests_name` varchar(255),
  `royalChests_price` integer
);

CREATE TABLE `gems` (
  `gems_id` varchar(255) PRIMARY KEY,
  `gems_variant` varchar(255),
  `gems_price` integer
);

CREATE TABLE `gold` (
  `gold_id` varchar(255) PRIMARY KEY,
  `gold_variant` varchar(255),
  `gold_price` integer
);

CREATE TABLE `creatorBoost` (
  `creatorBoost_id` varchar(255) PRIMARY KEY,
  `creatorBoost_code` varchar(255)
);

CREATE TABLE `collection` (
  `collection_id` varchar(255) PRIMARY KEY,
  `decks` varchar(255),
  `emotesNtowerskins` varchar(255),
  `magicItems` varchar(255),
  `bannersNbadges` varchar(255)
);

CREATE TABLE `deck` (
  `deck_id` varchar(255) PRIMARY KEY,
  `card1` varchar(255),
  `card2` varchar(255),
  `card3` varchar(255),
  `card4` varchar(255),
  `card5` varchar(255),
  `card6` varchar(255),
  `card7` varchar(255),
  `card8` varchar(255),
  `deck_avgElixir` integer
);

CREATE TABLE `emoteNtowerSkin` (
  `emoteNtowerSkin_id` varchar(255) PRIMARY KEY,
  `emotes` varchar(255),
  `towerskin` varchar(255)
);

CREATE TABLE `magicItems` (
  `magicItems_id` varchar(255) PRIMARY KEY,
  `magicItems_name` varchar(255),
  `magicItems_qty` integer
);

CREATE TABLE `bannerNbadges` (
  `bannerNbadges_id` varchar(255) PRIMARY KEY,
  `banners` varchar(255),
  `badges` varchar(255)
);

CREATE TABLE `card` (
  `card_id` varchar(255) PRIMARY KEY,
  `card_name` varchar(255),
  `card_rarity` varchar(255),
  `card_elixir` varchar(255),
  `card_level` integer,
  `card_type` varchar(255),
  `card_hp` integer,
  `card_damage` integer,
  `card_count` integer,
  `card_countmax` integer
);

CREATE TABLE `emote` (
  `emote_id` varchar(255) PRIMARY KEY,
  `emote_name` varchar(255)
);

CREATE TABLE `towerSkin` (
  `towerSkin_id` varchar(255) PRIMARY KEY,
  `towerSkin_name` varchar(255)
);

CREATE TABLE `banner` (
  `banner_id` varchar(255) PRIMARY KEY,
  `banner_name` varchar(255)
);

CREATE TABLE `badge` (
  `badge_id` varchar(255) PRIMARY KEY,
  `badge_name` varchar(255)
);

CREATE TABLE `battle` (
  `battle_id` varchar(255) PRIMARY KEY,
  `chest` varchar(255),
  `tournamentHub` varchar(255),
  `bannerBox` varchar(255),
  `battle` varchar(255),
  `dailyTasks` varchar(255),
  `player_profile` varchar(255),
  `social` varchar(255),
  `newsRoyale` varchar(255),
  `passroyalemenu` varchar(255),
  `options` varchar(255)
);

CREATE TABLE `social` (
  `social_id` varchar(255) PRIMARY KEY,
  `social_tag` varchar(255),
  `social_name` varchar(255),
  `social_deck` varchar(255),
  `social_tier` varchar(255),
  `social_tierRating` integer,
  `social_currentTrophy` integer,
  `social_previousTier` varchar(255),
  `social_clan` varchar(255),
  `social_badge` varchar(255),
  `social_win` integer,
  `social_threecrownwins` integer,
  `social_highestTrophies` integer,
  `social_cardsfound` integer,
  `social_totaldonations` integer,
  `social_favcard` varchar(255)
);

CREATE TABLE `clan` (
  `clan_id` varchar(255) PRIMARY KEY,
  `clan_name` varchar(255),
  `clan_desc` varchar(255),
  `clan_trophy` varchar(255),
  `clan_member` varchar(255)
);

CREATE TABLE `events` (
  `events_id` varchar(255) PRIMARY KEY,
  `event_name` varchar(255),
  `event_fee` integer,
  `event_player` varchar(255)
);

CREATE TABLE `chests` (
  `chests_id` varchar(255) PRIMARY KEY,
  `chests_name` varchar(255),
  `chests_time` integer,
  `chests_costgem` integer,
  `chests_chestkey` integer,
  `chests_gold` integer,
  `chests_gem` integer,
  `chests_commonCardcount` integer,
  `chests_commonCardname` varchar(255),
  `chests_rareCardcount` integer,
  `chests_rareCardname` varchar(255),
  `chests_epicCardcount` integer,
  `chests_epicCardname` varchar(255),
  `chests_legendaryCardcount` integer,
  `chests_legendaryCardname` varchar(255),
  `chests_championCardcount` integer,
  `chests_championCardname` varchar(255)
);

CREATE TABLE `options` (
  `options_id` varchar(255) PRIMARY KEY,
  `battleLog` varchar(255),
  `leaderboards` varchar(255),
  `tvRoyale` varchar(255),
  `trainingCamp` varchar(255),
  `tournaments` varchar(255),
  `settings` varchar(255)
);

CREATE TABLE `battleLog` (
  `battleLog_id` varchar(255) PRIMARY KEY,
  `battleResult` varchar(255),
  `scoreResult` varchar(255),
  `trophyPlus` integer,
  `trophyMinus` integer
);

CREATE TABLE `Leaderboards` (
  `leaderboards_id` varchar(255) PRIMARY KEY,
  `globalplayerleaderboards` varchar(255),
  `localplayerleaderboards` varchar(255),
  `globalclanleaderboards` varchar(255),
  `localclanleaderboards` varchar(255),
  `globalclanwarsleaderboards` varchar(255),
  `localclanwarsleaderboards` varchar(255)
);

CREATE TABLE `globalplayerLeaderboards` (
  `globalplayerLeaderboards_id` varchar(255) PRIMARY KEY,
  `globalplayerName` varchar(255),
  `globalplayerClan` varchar(255),
  `globalplayerTier` varchar(255),
  `globalplayerTierRatings` integer
);

CREATE TABLE `localplayerLeaderboards` (
  `localplayerLeaderboards_id` varchar(255) PRIMARY KEY,
  `localplayerName` varchar(255),
  `localplayerClan` varchar(255),
  `locarplayerTier` varchar(255),
  `localplayerTierRatings` integer
);

CREATE TABLE `globalclanLeaderboards` (
  `globalclanLeaderboards_id` varchar(255) PRIMARY KEY,
  `globalclanName` varchar(255),
  `globalclanTierRatings` integer,
  `globalclanTrophy` integer
);

CREATE TABLE `localclanLeaderboards` (
  `localclanLeaderboards_id` varchar(255) PRIMARY KEY,
  `localclanName` varchar(255),
  `localclanTierRatings` integer,
  `localclanTrophy` integer
);

CREATE TABLE `globalclanwarsLeaderboards` (
  `globalclanwarsLeaderboards_id` varchar(255) PRIMARY KEY,
  `globalclanwarsName` varchar(255),
  `globalclanwarsTrophy` integer
);

CREATE TABLE `localclanwarsLeaderboards` (
  `localclanwarsLeaderboards_id` varchar(255) PRIMARY KEY,
  `localclanwarsName` varchar(255),
  `localclanwarsTrophy` integer
);

```
## ALTER TABLE
ALTER TABLE is used to add, remove, or modify columns in a database.

```sql
ALTER TABLE `playerprofile` ADD FOREIGN KEY (`player_clan`) REFERENCES `clan` (`clan_id`);

ALTER TABLE `passRoyale` ADD FOREIGN KEY (`passRoyale_id`) REFERENCES `shop` (`passroyale`);

ALTER TABLE `eventBundle` ADD FOREIGN KEY (`eventBundle_id`) REFERENCES `shop` (`eventbundle`);

ALTER TABLE `specialOffer` ADD FOREIGN KEY (`specialOffer_id`) REFERENCES `shop` (`specialoffer`);

ALTER TABLE `trader` ADD FOREIGN KEY (`trader_id`) REFERENCES `shop` (`trader`);

ALTER TABLE `dailyDeals` ADD FOREIGN KEY (`dailyDeals_id`) REFERENCES `shop` (`dailydeals`);

ALTER TABLE `emoteShop` ADD FOREIGN KEY (`emoteShop_id`) REFERENCES `shop` (`emotes`);

ALTER TABLE `royalChests` ADD FOREIGN KEY (`royalChests_id`) REFERENCES `shop` (`royalchest`);

ALTER TABLE `gems` ADD FOREIGN KEY (`gems_id`) REFERENCES `shop` (`gems`);

ALTER TABLE `gold` ADD FOREIGN KEY (`gold_id`) REFERENCES `shop` (`gold`);

ALTER TABLE `creatorBoost` ADD FOREIGN KEY (`creatorBoost_id`) REFERENCES `shop` (`creatorboost`);

ALTER TABLE `playerprofile` ADD FOREIGN KEY (`player_tag`) REFERENCES `passRoyale` (`passRoyale_buyer`);

ALTER TABLE `playerprofile` ADD FOREIGN KEY (`player_tag`) REFERENCES `eventBundle` (`eventBundle_buyer`);

ALTER TABLE `deck` ADD FOREIGN KEY (`deck_id`) REFERENCES `collection` (`decks`);

ALTER TABLE `emoteNtowerSkin` ADD FOREIGN KEY (`emoteNtowerSkin_id`) REFERENCES `collection` (`emotesNtowerskins`);

ALTER TABLE `magicItems` ADD FOREIGN KEY (`magicItems_id`) REFERENCES `collection` (`magicItems`);

ALTER TABLE `bannerNbadges` ADD FOREIGN KEY (`bannerNbadges_id`) REFERENCES `collection` (`bannersNbadges`);

ALTER TABLE `card` ADD FOREIGN KEY (`card_id`) REFERENCES `deck` (`cards`);

ALTER TABLE `emote` ADD FOREIGN KEY (`emote_id`) REFERENCES `emoteNtowerSkin` (`emotes`);

ALTER TABLE `towerSkin` ADD FOREIGN KEY (`towerSkin_id`) REFERENCES `emoteNtowerSkin` (`towerskin`);

ALTER TABLE `banner` ADD FOREIGN KEY (`banner_id`) REFERENCES `bannerNbadges` (`banners`);

ALTER TABLE `badge` ADD FOREIGN KEY (`badge_id`) REFERENCES `bannerNbadges` (`badges`);

ALTER TABLE `chests` ADD FOREIGN KEY (`chests_id`) REFERENCES `battle` (`chest`);

ALTER TABLE `playerprofile` ADD FOREIGN KEY (`player_tag`) REFERENCES `battle` (`player_profile`);

ALTER TABLE `social` ADD FOREIGN KEY (`social_id`) REFERENCES `battle` (`social`);

ALTER TABLE `options` ADD FOREIGN KEY (`options_id`) REFERENCES `battle` (`options`);

ALTER TABLE `social` ADD FOREIGN KEY (`social_clan`) REFERENCES `clan` (`clan_id`);

ALTER TABLE `playerprofile` ADD FOREIGN KEY (`player_tag`) REFERENCES `clan` (`clan_member`);

ALTER TABLE `playerprofile` ADD FOREIGN KEY (`player_tag`) REFERENCES `events` (`event_player`);

ALTER TABLE `battleLog` ADD FOREIGN KEY (`battleLog_id`) REFERENCES `options` (`battleLog`);

ALTER TABLE `Leaderboards` ADD FOREIGN KEY (`leaderboards_id`) REFERENCES `options` (`leaderboards`);

ALTER TABLE `globalplayerLeaderboards` ADD FOREIGN KEY (`globalplayerLeaderboards_id`) REFERENCES `Leaderboards` (`globalplayerleaderboards`);

ALTER TABLE `localplayerLeaderboards` ADD FOREIGN KEY (`localplayerLeaderboards_id`) REFERENCES `Leaderboards` (`localplayerleaderboards`);

ALTER TABLE `globalclanLeaderboards` ADD FOREIGN KEY (`globalclanLeaderboards_id`) REFERENCES `Leaderboards` (`globalclanleaderboards`);

ALTER TABLE `localclanLeaderboards` ADD FOREIGN KEY (`localclanLeaderboards_id`) REFERENCES `Leaderboards` (`localclanleaderboards`);

ALTER TABLE `globalclanwarsLeaderboards` ADD FOREIGN KEY (`globalclanwarsLeaderboards_id`) REFERENCES `Leaderboards` (`globalclanwarsleaderboards`);

ALTER TABLE `localclanwarsLeaderboards` ADD FOREIGN KEY (`localclanwarsLeaderboards_id`) REFERENCES `Leaderboards` (`localclanwarsleaderboards`);

ALTER TABLE clan MODIFY COLUMN clan_member integer;

```

## DROP TABLE
DROP TABLE is used to delete a database.

```sql
DROP TABLE clan ;
```
# No.3
DML stands for Data Manipulation Language is a set of query to manipulate the database, include INSERT INTO, UPDATE, and DELETE. I have given some examples of them below based on my project.


## Use Case

No | Use Case | Priority Rate
---|---|---
1 | Player is able to add cards into a deck | 90
2 | Player is able to remove cards from a deck | 90
3 | Player is able to know how many cards basen on the elixir | 70
4 | Player is able to update cards in the deck | 90
5 | Player is able to add friends | 70

### 1
```sql

```
### 2

### 3

### 4

### 5

# No.4
DQL stands for Data Query Language is a statement to get data from database to perform operations with it. We usually use SELECT command to show a certain database and combine it with some operations. They are GROUP BY, ORDER BY, AVERAGE, MAX, MIN, and many more. I have given some examples of them below based on my project.

1) Who are the players in Local Clan Leaderboards?
2) How many cards based on the rarity?
3) What is the most durable troop card (Descending Sort the card_hp column)?
4) How many players that purchase Pass Royale?
5) How many gems that game have been received from players purchase Event Bundle?

## 1) Who are the players in Local Clan Leaderboards?

# No.5

