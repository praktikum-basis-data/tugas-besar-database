CREATE TABLE `clubs` (
  `club_id` int PRIMARY key auto_increment,
  `club_name` varchar(255),
  `club_stadium` varchar(255) unique,
  `club_yearEst` varchar(4)
);

alter table clubs auto_increment=1;



CREATE TABLE `players` (
  `player_id` int PRIMARY key auto_increment,
  `player_name` varchar(255),
  `player_nationality` varchar(255),
  `player_birth` date,
  `player_club` int,
  `player_pos` varchar(255),
  `player_PLdebut` date,
  `player_apps` int,
  `player_goals` int,
  `player_cleansheets` int,
  `player_wins` int,
  `player_losses` int
);

alter table players auto_increment=1;

ALTER TABLE `players` ADD FOREIGN KEY (`player_club`) REFERENCES `clubs` (`club_id`);

CREATE TABLE `managers` (
  `manager_id` int primary key auto_increment,
  `manager_name` varchar(255),
  `manager_club` int unique
);

alter table managers auto_increment=1;

ALTER TABLE `managers` ADD FOREIGN KEY (`manager_club`) REFERENCES `clubs` (`club_id`);




CREATE TABLE `tables` (
  `tables_id` varchar(20) primary key,
  `tables_season` varchar(10),
  `club_id` int,
  `club_wins` int,
  `club_draws` int,
  `club_losses` int,
  `club_scored` int,
  `club_conceded` int,
  `club_points` int
);

ALTER TABLE `tables` ADD FOREIGN KEY (`club_id`) REFERENCES `clubs` (`club_id`);

create table stats (
stats_id varchar(30) primary key,
poss_home varchar(2),
poss_away varchar(2),
shotsontarget_home varchar(3),
shotsontarget_away varchar(3),
shots_home varchar(3),
shots_away varchar(3),
touches_home varchar(4),
touches_away varchar(4),
passes_home varchar(4),
passes_away varchar(4),
tackles_home varchar(3),
tackles_away varchar(3),
clearances_home varchar(3),
clearances_away varchar(3),
corners_home varchar(2),
corners_away varchar(2),
offsides_home varchar(2),
offsides_away varchar(2)
);

CREATE TABLE `results` (
  `results_id` varchar(255) PRIMARY KEY,
  `result_season` varchar(10),
  `club_home` int,
  `club_away` int,
  `home_score` int,
  `away_score` int,
  `match_date` date,
  `match_stats` varchar(255)
);

ALTER TABLE `results` ADD FOREIGN KEY (`club_home`) REFERENCES `clubs` (`club_id`);

ALTER TABLE `results` ADD FOREIGN KEY (`club_away`) REFERENCES `clubs` (`club_id`);

ALTER TABLE `results` ADD FOREIGN KEY (`match_stats`) REFERENCES `stats` (`stats_id`);


CREATE TABLE `fixtures` (
  `fixtures_id` varchar(255) PRIMARY KEY,
  `fixture_season` varchar(10),
  `club_home` int,
  `club_away` int,
  `match_date` date
);

ALTER TABLE `fixtures` ADD FOREIGN KEY (`club_home`) REFERENCES `clubs` (`club_id`);

ALTER TABLE `fixtures` ADD FOREIGN KEY (`club_away`) REFERENCES `clubs` (`club_id`);

CREATE TABLE `users` (
  `user_id` int PRIMARY key auto_increment,
  `user_email` varchar(255),
  `user_password` varchar(255),
  `user_name` varchar(255),
  `user_gender` varchar(255),
  `user_birthdate` date,
  `user_favClub` int
);

ALTER TABLE `users` ADD FOREIGN KEY (`user_favClub`) REFERENCES `clubs` (`club_id`);

CREATE TABLE `news` (
  `news_id` int PRIMARY key auto_increment,
  `news_title` varchar(255),
  `news_link` varchar(255)
);







