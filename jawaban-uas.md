# No.1

## Use Case

| No | Use Case | Priority Rate (%) |
| --- | --- | --- |
| 1 | Users know the result of all matches | 100 |
| 2 | Users know the fixtures of all matches | 100 |
| 3 | Users know the club standings | 100 |
| 4 | Users know the clubs general info | 70 |
| 5 | Users know the clubs squad | 90 |
| 6 | Users know the managers general info | 70 |
| 7 | Users know the players general info | 70 |
| 8 | Users know the player stats | 80 |
| 9 | Users are able to login | 50 |
| 10 | Users get news about Premier League | 60 |
| 11 | Users get news which related to user favorite club | 60 |
| 12 | Board know number of app users | 90 |
| 13 | Board know favourite club of app users | 80 |

# No.2

## C -> Create
```sql
CREATE TABLE `clubs` (
  `club_id` int PRIMARY key auto_increment,
  `club_name` varchar(255),
  `club_stadium` varchar(255) unique,
  `club_yearEst` varchar(4)
);
```

## R -> Read
```sql
INSERT INTO `clubs`
VALUES

```

## U -> Update
```sql

```

## D -> Delete
```sql

```

# No.3

# No.4

# No.5

# No.6

# No.7

# No.8

# No.9

# No.10

# No.11

